import { NavBar } from 'components/NavBar';
import { Admin } from 'pages/Admin';
import { Catalog } from 'pages/Catalog';
import { Home } from 'pages/Home';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

export const MyRoutes = () => {
  return (
    <BrowserRouter>
      <NavBar />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/catalog" element={<Catalog />} />
        <Route path="/admin" element={<Admin />} />
      </Routes>
    </BrowserRouter>
  );
};
